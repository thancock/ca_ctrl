FROM resin/rpi-raspbian:jessie
ENV LANG C.UTF-8

RUN apt-get update && apt-get install -y apt-transport-https

RUN apt-get install pigpio
RUN apt-get install python3
RUN apt-get install python-pigpio python3-pigpio

COPY pi_hifi_ctrl /

ADD run.sh /
RUN chmod +x /run.sh

CMD ["/run.sh"]